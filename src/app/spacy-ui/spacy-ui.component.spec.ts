import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpacyUiComponent } from './spacy-ui.component';

describe('SpacyUiComponent', () => {
  let component: SpacyUiComponent;
  let fixture: ComponentFixture<SpacyUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpacyUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpacyUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
