import {Component, OnInit} from '@angular/core';
import {SpacyService} from '../spacy.service';
import {Subject, Observable, of} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, min, switchMap} from 'rxjs/operators';
import {ENGLISH, GERMAN, SpacyData, SpacyDataArc} from '../spacyData';

function mapArcToNewSentence(arcs: SpacyDataArc[], lastIndex: number, jumps: number[]): SpacyDataArc[] {
  return arcs.map(elem => {
    elem.end = elem.end - (lastIndex + 1 + jumps.filter(jump => jump <= elem.end).length);
    elem.start = elem.start - (lastIndex + 1 + jumps.filter(jump => jump <= elem.start).length);
    return elem;
  });
}

@Component({
  selector: 'app-spacy-ui',
  templateUrl: './spacy-ui.component.html',
  styleUrls: ['./spacy-ui.component.scss']
})
export class SpacyUiComponent implements OnInit {
  jsonResult$: Observable<SpacyData[]>;
  language = 'de';
  private nlpTerm = new Subject<string>();

  constructor(private spacyService: SpacyService) {
    const data: SpacyData[] = [];
    this.jsonResult$ = of(data);
  }

  // Push a search term into the observable stream.
  search(term: string): void {
    this.nlpTerm.next(term);
  }

  splitSentences(data: SpacyData): SpacyData[] {
    const splitSentences: SpacyData[] = [];
    let lastIndex = -1;
    data.words.forEach((word, index) => {
      if (['.', '!', '?'].indexOf(word.text.charAt(word.text.length - 1)) >= 0) {
        const jumps: number[] = [];
        splitSentences.push({
          words: data.words.filter((elem, i) => {
            if (i > lastIndex && i <= index) {
              if (elem.tag !== '_SP') {
                return true;
              }
              jumps.push(i);
            }
            return false;
          }),
          arcs: mapArcToNewSentence(data.arcs.filter(elem => elem.label.length &&
            ((elem.end > lastIndex && elem.end <= index) ||
              (elem.start > lastIndex && elem.start <= index))),
            lastIndex, jumps
          )
        });
        lastIndex = index;
      }
    });
    const jumpsOuter: number[] = [];
    const words = data.words.filter((elem, i) => {
      if (i > lastIndex) {
        if (elem.tag !== '_SP') {
          return true;
        }
        jumpsOuter.push(i);
      }
      return false;
    });
    if (words.length > 0) {
      splitSentences.push({
        words,
        arcs: mapArcToNewSentence(
          data.arcs.filter(elem => elem.label.length && (elem.end > lastIndex || elem.start > lastIndex)),
          lastIndex, jumpsOuter
        )
      });
    }
    return splitSentences;
  }

  getDescription(type: string): string {
    if (this.language === 'en') {
      const dict: { [key: string]: any } = ENGLISH.TAGS;
      return dict[type] || 'No Description';
    } else {
      const dict: { [key: string]: any } = GERMAN.TAGS;
      return dict[type] || 'Keine Beschreibung';
    }
  }

  getKeyPairParserLabels(): { key: string, value: string }[] {
    let dict: { [key: string]: any };
    if (this.language === 'en') {
      dict = ENGLISH.PARSER_LABELS;
    } else {
      dict = GERMAN.PARSER_LABELS;
    }
    const result = [];
    for (const key of Object.keys(dict)) {
      result.push({
        key,
        value: dict[key] || ''
      });
    }
    return result;
  }

  ngOnInit(): void {
    this.jsonResult$ = this.nlpTerm.pipe(
      debounceTime(1_000),
      switchMap((term: string) => this.spacyService.requestNLPResult(term, this.language)
        .pipe(
          map(this.splitSentences)
        ))
    );
  }
}
