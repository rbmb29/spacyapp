import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {SpacyData} from './spacyData';

@Injectable({
  providedIn: 'root'
})
export class SpacyService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T): (_: any) => Observable<T> {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  requestNLPResult(message: string, lang: string): Observable<SpacyData> {
    return this.http.post<SpacyData>('/api',
      {
        text: message.substr(0, 1_000),
        model: lang
      },
      this.httpOptions
    ).pipe(
      catchError(this.handleError<SpacyData>('requestNLPResult', {words: [], arcs: []}))
    );
  }
}
