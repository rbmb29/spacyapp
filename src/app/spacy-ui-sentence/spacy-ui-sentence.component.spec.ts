import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpacyUiSentenceComponent } from './spacy-ui-sentence.component';

describe('SpacyUiSentenceComponent', () => {
  let component: SpacyUiSentenceComponent;
  let fixture: ComponentFixture<SpacyUiSentenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpacyUiSentenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpacyUiSentenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
