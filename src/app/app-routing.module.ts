import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SpacyUiComponent} from './spacy-ui/spacy-ui.component';

const routes: Routes = [
  {path: '', redirectTo: '/spacy', pathMatch: 'full'},
  {path: 'spacy', component: SpacyUiComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
