# SpaCyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

## Examples of this app

This app attempts to visualize the data from [spacy.frag.jetzt](https://spacy.frag.jetzt/dep).
The data is sent to the server using HTTP POST request and then transformed into visual overlays.

Empty input with language select:
![A Picture of language select](img/lang-select.png)

### Example of transformation with german text

![A Picture of german text analysis](img/deu-screen-1.png)
![A Picture of german text analysis](img/deu-screen-2.png)
![A Picture of german text analysis](img/deu-screen-3.png)

### Example of transformation with english text


![A Picture of english text analysis](img/en-screen-1.png)
![A Picture of english text analysis](img/en-screen-2.png)
